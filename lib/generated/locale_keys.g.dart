// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const calendar_january = 'calendar.january';
  static const calendar_february = 'calendar.february';
  static const calendar_march = 'calendar.march';
  static const calendar_april = 'calendar.april';
  static const calendar_may = 'calendar.may';
  static const calendar_june = 'calendar.june';
  static const calendar_july = 'calendar.july';
  static const calendar_august = 'calendar.august';
  static const calendar_september = 'calendar.september';
  static const calendar_october = 'calendar.october';
  static const calendar_november = 'calendar.november';
  static const calendar_december = 'calendar.december';
  static const calendar_monday = 'calendar.monday';
  static const calendar_tuesday = 'calendar.tuesday';
  static const calendar_wednesday = 'calendar.wednesday';
  static const calendar_thursday = 'calendar.thursday';
  static const calendar_friday = 'calendar.friday';
  static const calendar_saturday = 'calendar.saturday';
  static const calendar_sunday = 'calendar.sunday';
  static const calendar = 'calendar';

}
